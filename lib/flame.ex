defmodule Flame do
  @moduledoc """
  Documentation for Flame.
  """

  @doc """
  """
  @spec set_color(byte(), byte(), byte()) :: :ok
  def set_color(r, g, b) do
    Flame.Blinkt.set_pixels([
      {0, 0, 0},
      {r, g, b},
      {r, g, b},
      {r, g, b},
      {r, g, b},
      {r, g, b},
      {r, g, b},
      {0, 0, 0}
    ])
  end

  def set_color({r, g, b}), do: set_color(r, g, b)
end
