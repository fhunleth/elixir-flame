defmodule Flame.HSV do
  defstruct h: 0,
            s: 0,
            v: 0

  def new() do
    %__MODULE__{}
  end

  def new(h, s, v) do
    %__MODULE__{h: h, s: s, v: v}
  end

  defimpl Inspect, for: Flame.HSV do
    def inspect(%Flame.HSV{} = hsv, _opts) do
      "#HSV<#{hsv.h}, #{hsv.s}, #{hsv.v}>"
    end
  end
end
