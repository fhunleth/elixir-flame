defmodule Flame.Blinkt do
  use GenServer
  alias ElixirALE.GPIO
  alias Flame.Color

  @data_pin 23
  @clk_pin 24

  defmodule State do
    defstruct clk: nil,
              data: nil,
              brightness: 31,
              last_data: -1
  end

  @type brightness :: 0..31

  def start_link([]) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @spec set_brightness(brightness()) :: :ok
  def set_brightness(brightness) when brightness >= 0 and brightness < 32 do
    GenServer.call(__MODULE__, {:set_brightness, brightness})
  end

  @spec set_n_pixels(non_neg_integer(), Color.rgb()) :: :ok
  def set_n_pixels(count, pixel) do
    pixels = for _i <- 1..count, do: pixel
    set_pixels(pixels)
  end

  @spec set_pixels([Color.rgb()]) :: :ok
  def set_pixels(pixels) when is_list(pixels) do
    GenServer.call(__MODULE__, {:set_pixels, pixels})
  end

  def init(_args) do
    {:ok, clk} = GPIO.open(@clk_pin, :output)
    {:ok, data} = GPIO.open(@data_pin, :output)

    {:ok, %State{clk: clk, data: data}}
  end

  def handle_call({:set_brightness, brightness}, _from, state) do
    {:reply, :ok, %{state | brightness: brightness}}
  end

  def handle_call({:set_pixels, pixels}, _from, state) do
    state
    |> write_sof()
    |> write(pixels_to_data(state.brightness, pixels))
    |> write_eof()

    {:reply, :ok, state}
  end

  defp pixels_to_data(brightness, pixels) do
    global_brightness = 0xE0 + brightness

    Enum.flat_map(pixels, fn {r, g, b} -> [global_brightness, b, g, r] end)
  end

  defp write_sof(state) do
    write(state, [0, 0, 0, 0])
  end

  defp write_eof(state) do
    write(state, [0, 0, 0, 0, 0])
  end

  defp write(state, data) do
    Enum.reduce(data, state, &write_byte/2)
  end

  defp write_byte(value, state) do
    <<v7::size(1), v6::size(1), v5::size(1), v4::size(1), v3::size(1), v2::size(1), v1::size(1),
      v0::size(1)>> = <<value>>

    state
    |> write_bit(v7)
    |> write_bit(v6)
    |> write_bit(v5)
    |> write_bit(v4)
    |> write_bit(v3)
    |> write_bit(v2)
    |> write_bit(v1)
    |> write_bit(v0)
  end

  defp write_bit(%{last_data: value} = state, value) do
    :ok = GPIO.write(state.clk, 1)
    :ok = GPIO.write(state.clk, 0)
    state
  end

  defp write_bit(state, value) do
    :ok = GPIO.write(state.data, value)
    :ok = GPIO.write(state.clk, 1)
    :ok = GPIO.write(state.clk, 0)
    %{state | last_data: value}
  end
end
