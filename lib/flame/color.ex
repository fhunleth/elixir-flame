defmodule Flame.Color do
  @type rgb :: {byte(), byte(), byte()}
  @type degrees :: number()
  @type percent :: number()

  defguard is_normalized_degrees(d) when is_number(d) and d >= 0 and d < 360
  defguard is_percent(d) when is_number(d) and d >= 0 and d <= 1

  @doc """
  Convert an HSV color to RGB.
  """
  @spec hsv2rgb(degrees(), percent(), percent()) :: rgb()
  def hsv2rgb(_h, s, v) when s <= 0 and is_percent(v) do
    a = round(255 * v)
    {a, a, a}
  end

  def hsv2rgb(h, s, v) when is_normalized_degrees(h) and is_percent(s) and is_percent(v) do
    h_sixths = h / 60
    h_sector = round(:math.floor(h_sixths))
    h_offset = h_sixths - h_sector

    x = round(255 * v * (1 - s))
    y = round(255 * v * (1 - s * h_offset))
    z = round(255 * v * (1 - s * (1 - h_offset)))
    w = round(255 * v)

    hsv_sector_to_rgb(h_sector, x, y, z, w)
  end

  defp hsv_sector_to_rgb(0, x, _y, z, w), do: {w, z, x}
  defp hsv_sector_to_rgb(1, x, y, _z, w), do: {y, w, x}
  defp hsv_sector_to_rgb(2, x, _y, z, w), do: {x, w, z}
  defp hsv_sector_to_rgb(3, x, y, _z, w), do: {x, y, w}
  defp hsv_sector_to_rgb(4, x, _y, z, w), do: {z, x, w}
  defp hsv_sector_to_rgb(5, x, y, _z, w), do: {w, x, y}
end
